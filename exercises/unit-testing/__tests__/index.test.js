describe('Object.assign testing', () => {
  test('should copying properties to target object', () => {
    const target = { a: 'a', d: true };
    const source1 = { a: 1, b: 1, c: 1 };
    const source2 = { b: 2, c: 2 };
    const source3 = { c: 3 };
    const result = Object.assign(target, source1, source2, source3);
    expect(result).toEqual({
      a: 1, b: 2, c: 3, d: true,
    });
    expect(result).toBe(target);
  });

  test('empty objects', () => {
    let emptyObject = {};
    const obj = { a: 'a' };
    expect(Object.assign(obj, emptyObject)).toEqual(obj);
    expect(Object.assign(emptyObject, obj)).toEqual(obj);
    emptyObject = {};
    expect(Object.assign(emptyObject, emptyObject)).toEqual(emptyObject);
  });

  test('different types', () => {
    const emptyObject = {};
    expect(Object.assign(emptyObject, 1, 'abc', [5, 6], true, null, undefined))
      .toEqual({ 0: 5, 1: 6, 2: 'c' });
  });

  test('properties on the prototype chain and non-enumerable properties cannot be copied', () => {
    const emptyObject = {};
    const source = Object.create({ prototypeChainProperty: 'value' }, {
      nonEnumerable: { value: 1 },
      enumerable: { value: 2, enumerable: true },
    });
    expect(Object.assign(emptyObject, source)).toEqual({ enumerable: 2 });
  });
});
