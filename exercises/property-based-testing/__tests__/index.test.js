const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('sort', () => {
  it('should get sort array', () => {
    fc.assert(fc.property(fc.int32Array(),
      (arr) => {
        expect(sort(arr)).toBeSorted();
      }));
  });
  it('idempotency', () => {
    fc.assert(fc.property(fc.int32Array(),
      (arr) => {
        expect(sort(sort(arr))).toEqual(sort(arr));
      }));
  });
  it('length shouldn\'t change', () => {
    fc.assert(fc.property(fc.int32Array(),
      (arr) => {
        expect(sort(arr)).toHaveLength(arr.length);
      }));
  });
  it('number set shouldn\'t change', () => {
    fc.assert(fc.property(fc.int32Array(),
      (arr) => {
        const initialSet = new Set(arr);
        const sortedArraySet = new Set(sort(arr));
        expect(sortedArraySet).toEqual(initialSet);
      }));
  });
});
// END
