const faker = require('faker');

// BEGIN
describe('faker.helpers.createTransaction', () => {
  it('should create correct transaction', () => {
    const transaction = faker.helpers.createTransaction();
    expect(transaction).toMatchObject({
      amount: expect.stringMatching(/^\d+\.\d\d$/),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.stringMatching(/^[a-z]+ *[a-z]* Account \d{4}$/i),
      type: expect.stringMatching(/^(payment)|(invoic)|(withdrawal)|(deposit)$/),
      account: expect.stringMatching(/^\d{8}$/),
    });
    expect(Object.keys(transaction)).toHaveLength(6);
  });

  it('should create uniq transaction', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();
    expect(transaction1).not.toMatchObject(transaction2);
  });
});
// END
