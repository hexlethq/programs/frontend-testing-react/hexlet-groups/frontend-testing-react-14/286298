const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
describe('Http requests', () => {
  beforeAll(() => {
    nock.disableNetConnect();
  });

  afterEach(() => {
    nock.cleanAll();
  });

  it('get request', async () => {
    const body = { id: '123ABC' };
    const rootUrl = 'https://www.google.com';
    const path = '/user';
    const scope = nock(rootUrl).get(path).reply(200, body);
    const response = await get(`${rootUrl}${path}`);
    expect(response).toEqual(body);
    expect(scope.isDone()).toBe(true);
  });

  it('post request', async () => {
    const body = { id: '123ABC' };
    const rootUrl = 'https://www.google.com';
    const path = '/user/path';
    const scope = nock(rootUrl).post(path).reply(200, body);
    const response = await post(`${rootUrl}${path}`);
    expect(response).toEqual(body);
    expect(scope.isDone()).toBe(true);
  });

  it('error requests', async () => {
    const rootUrl = 'https://www.google.com';
    const path = '/error';
    const fullPath = `${rootUrl}${path}`;
    const scope = nock(rootUrl)
      .get(path).reply(404)
      .post(path)
      .reply(500);
    const errors = await Promise.all([
      get(fullPath).catch((err) => err),
      post(fullPath).catch((err) => err),
    ]).then(([getError, postError]) => ([getError.message, postError.message]));
    expect(errors).toEqual([
      'Request failed with status code 404',
      'Request failed with status code 500',
    ]);
    expect(scope.isDone()).toBe(true);
  });
});
// END
