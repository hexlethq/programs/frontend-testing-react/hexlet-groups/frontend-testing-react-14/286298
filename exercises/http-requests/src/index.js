const axios = require('axios');

// BEGIN
const get = (url) => axios.get(url).then((response) => response.data);
const post = (url) => axios.post(url).then((response) => response.data);
// END

module.exports = { get, post };
