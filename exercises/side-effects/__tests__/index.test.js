const fs = require('fs');
const { upVersion } = require('../src/index.js');

// BEGIN
describe('upVersion', () => {
  const packageFilePath = `${__dirname}/../__fixtures__/package.json`;
  let initialFileContent;

  beforeEach(() => {
    initialFileContent = fs.readFileSync(packageFilePath, 'utf-8');
  });

  afterEach(() => {
    fs.writeFileSync(packageFilePath, initialFileContent, 'utf-8');
  });

  it('should correct update', () => {
    let packageContent;

    upVersion(packageFilePath, 'major');
    packageContent = fs.readFileSync(packageFilePath, 'utf-8').replace(/\s/g, '');
    expect(packageContent).toEqual('{"version":"2.0.0","package":"3.0.0"}');

    upVersion(packageFilePath, 'minor');
    packageContent = fs.readFileSync(packageFilePath, 'utf-8').replace(/\s/g, '');
    expect(packageContent).toEqual('{"version":"2.1.0","package":"3.1.0"}');

    upVersion(packageFilePath, 'patch');
    packageContent = fs.readFileSync(packageFilePath, 'utf-8').replace(/\s/g, '');
    expect(packageContent).toEqual('{"version":"2.1.1","package":"3.1.1"}');

    upVersion(packageFilePath);
    packageContent = fs.readFileSync(packageFilePath, 'utf-8').replace(/\s/g, '');
    expect(packageContent).toEqual('{"version":"2.1.2","package":"3.1.2"}');
  });
});
// END
