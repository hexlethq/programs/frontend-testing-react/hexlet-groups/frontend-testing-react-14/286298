const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, [3, [4]], 5]];
assert.notEqual(flattenDepth(array, 0), array, 'Should return new object');
assert.deepEqual(flattenDepth(array), [1, 2, [3, [4]], 5], 'Default depth equal one');
assert.deepEqual(flattenDepth(array, 1), [1, 2, [3, [1]], 5], 'Depth=1');
assert.deepEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5], 'Depth=2');
assert.deepEqual(flattenDepth(array, 99999999999999999999), [1, 2, 3, 4, 5], 'Big depth');
assert.deepEqual(flattenDepth([], 2), [], 'Should flat empty arrays');
assert.deepEqual(flattenDepth(array, -1), array, 'If the depth is incorrect, it is counted as zero');
// END
