const fs = require('fs');
const path = require('path');

// BEGIN
const updateTypes = {
  patch: 'patch',
  minor: 'minor',
  major: 'major',
};

function getUpdatedVersion(version, updateType) {
  const separator = '.';
  const [major, minor, patch] = version.split(separator);
  if (updateType === updateTypes.major) {
    return [Number(major) + 1, 0, 0].join(separator);
  }
  if (updateType === updateTypes.minor) {
    return [major, Number(minor) + 1, 0].join(separator);
  }
  return [major, minor, Number(patch) + 1].join(separator);
}

function updatePackage(packageContent, updateType) {
  const versionObject = JSON.parse(packageContent);
  const packages = Object.keys(versionObject);
  packages.forEach((packageName) => {
    const version = versionObject[packageName];
    versionObject[packageName] = getUpdatedVersion(version, updateType);
  });
  return JSON.stringify(versionObject);
}

function upVersion(filePath, updateType = updateTypes.patch) {
  const fullPath = path.resolve(filePath);
  const fileContent = fs.readFileSync(fullPath, 'utf-8');
  const updatedJson = updatePackage(fileContent, updateType);
  fs.writeFileSync(fullPath, updatedJson, 'utf-8');
}
// END

module.exports = { upVersion };
